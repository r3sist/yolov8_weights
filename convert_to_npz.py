import torch
import numpy as np

for i in ['n', 'm', 's', 'l', 'x']:
    state_dict = torch.load(f'./yolov8{i}.pt')
    weights = state_dict['model'].state_dict()
    np_weights = {key.replace('model.model', 'model'): value.numpy() for key, value in weights.items()}
    np.savez_compressed(f'./yolov8{i}.npz', **np_weights)