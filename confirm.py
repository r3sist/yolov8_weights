import torch
from safetensors.torch import load_file
from extra.utils import download_file
import numpy as np

for i in ['n', 'm', 's', 'l', 'x']:
    path = f'./yolov8{i}.pt'
    safe_path = f'./yolov8{i}.safetensors'
    download_file(f'https://github.com/ultralytics/assets/releases/download/v0.0.0/yolov8{i}.pt', f'./yolov8{i}.pt')
    state_dict = torch.load(path)['model'].state_dict().items()
    safe_dict = load_file(safe_path)
    
    for k, v in state_dict:
        np.testing.assert_allclose(v.numpy(), safe_dict[k].numpy(), atol=5e-4, rtol=1e-5)
    print(path, 'done')