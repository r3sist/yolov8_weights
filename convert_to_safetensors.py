
import torch
from safetensors.torch import save_file
from extra.utils import download_file

for i in ['n', 'm', 's', 'l', 'x']:
    download_file(f'https://github.com/ultralytics/assets/releases/download/v0.0.0/yolov8{i}.pt', f'./yolov8{i}.pt')
    state_dict = torch.load(f'./yolov8{i}.pt')
    weights = state_dict['model'].state_dict()
    safe_weights = {key.replace('model.model', 'model'): value for key, value in weights.items()}
    save_file(safe_weights, filename=f'./yolov8{i}.safetensors')